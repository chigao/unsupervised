#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  9 17:23:03 2018

@author: cgao
"""

#import pandas as pd
import numpy as np
#import scipy
from scipy import stats
import math
import random
import matplotlib.pyplot as plt
import imageio
from functools import reduce
import pdb


def EuclidDistance(x1, x2):
    if len(x1) != len(x2):
        return math.inf
    else:
        return math.sqrt(sum((a-b)**2 for (a,b) in zip(x1,x2)))

# test
# EuclidDistance([0,0], [3,4])

def closetCenter(x, centers):
    cluster = 0
    center = centers[cluster]
    distance = EuclidDistance(x, center)
    for idx in range(1, len(centers)):
        temp = EuclidDistance(x, centers[idx])
        if temp < distance:
            cluster = idx
            center = centers[cluster]
            distance = temp
    return({"cluster":cluster, "center":center, "distance":distance})
# test
# closetCenter([0,0],[[1,1],[1,2],[2,1]])

def getCenter(cs):
    center = [0]*len(cs[0])
    for c in cs:
        center = [a+b for a,b in zip(center, c)]
    return([c/len(cs) for c in center])

# test
# getCenter([[0,0],[2,2]])

def kmean(k, xs, init = False, maxstep = 1000, minsteperr = 1e-3, plot = False, gifname = 'kmean.gif'):
    n = len(xs)
    d = len(xs[0])
    #check if k is > len(xs)
    if k > n:
        print("Error: number of cluster is larger than data size.")
        return(-9999)
    # initialize center
    if init != False:
        centers = init['centers']
    else:
        centers_ind = random.sample(range(0, n),k) 
        centers = [xs[centers_ind[i]] for i in range(k)]
    step = 1
    error = math.inf
    steperror = math.inf
 
    #plot if plot = True and d = 2
    if plot and d == 2:
        cmap = plt.cm.get_cmap('hsv', k+5) 
        xx = [x[0] for x in xs]
        yy = [x[1] for x in xs]
        plt.plot(xx, yy, 'o', color=cmap(0), markerfacecolor='None')
        for i in range(k):
            plt.plot(centers[i][0], centers[i][1], '^', color=cmap(i), markersize = 20)
            plt.plot(centers[i][0], centers[i][1], '+k')
        plt.axis('equal')
        plt.title("step = 0")
        ax = plt.gca()
        text = ""
        for iic in range(k):
            text += "["
            for iid in range(d):
                text += " %.2f"%centers[iic][iid] + " "
            text += "] "
        ax.text(0.01, 0.95, text,
             verticalalignment='top', horizontalalignment='left',
                transform=ax.transAxes, color='black', fontsize=10)

        plt.savefig('images/kmean_step_0.png')            
        plt.show()  
        # create gif
        images = []
        images.append(imageio.imread('images/kmean_step_0.png'))
 
    # clusters store the points in each cluster
    while (step < maxstep and steperror > minsteperr):
        # clusters = [[]]*k; clusters[0].append(0) will be applied to all member in the list...
        clusters = [[] for i in range(k)]
        newerror = 0
        # cluster step
        for x in xs:
            temp = closetCenter(x, centers)
            clusters[temp['cluster']].append(x)
            newerror += temp['distance']
            #print("Point [%3.2f, %3.2f] is in cluster %d defined by center [%3.2f, %3.2f]"%(x[0],x[1],temp['cluster'], centers[temp['cluster']][0],centers[temp['cluster']][1]))
        # update error, steperror and center (if necessary)
        newerror = newerror/float(n)
        print("newerror = %.3f, error = %.3f"%(newerror, error))
        if newerror <= error:
            steperror = error - newerror
            error = newerror
            centers = [getCenter(cluster) for cluster in clusters]
            print("step = %d; error = %.3f; steperror = %.3f"%(step, error, steperror))
            print("Centers: ", centers)
        else:
            print("Error won't decrease")
            break
        #plot if plot = True and d = 2
        if plot and d == 2:
            #cmap = plt.cm.get_cmap('hsv', k+5) 
            for i in range(k):
                xx = [x[0] for x in clusters[i]]
                yy = [x[1] for x in clusters[i]]
                plt.plot(xx, yy, 'o', color=cmap(i), markerfacecolor='None')
                plt.plot(centers[i][0], centers[i][1], '^', color=cmap(i), markersize = 20)
                plt.plot(centers[i][0], centers[i][1], '+k')
            plt.axis('equal')
            plt.title("step = %d, error = %.3f"%(step, error))
            ax = plt.gca()
            text = ""
            for iic in range(k):
                text += "["
                for iid in range(d):
                    text += " %.2f"%centers[iic][iid] + " "
                text += "] "
            ax.text(0.01, 0.95, text,
                 verticalalignment='top', horizontalalignment='left',
                    transform=ax.transAxes, color='black', fontsize=10)
            plt.savefig('images/kmean_step_%d.png'%step)            
            plt.show() 
            images.append(imageio.imread('images/kmean_step_%d.png'%step))
        # update step
        step += 1
    if step == maxstep:
        print("Reached maximum Step Size %d"%maxstep)
    else:
        print("Error decrease size is less than the threshold of %f"%minsteperr)
    if plot and d==2:
        imageio.mimsave(gifname, images, fps = 1)
    return(clusters)
#test




# EM
# for simplicity, assume the d-dimension prior distribution is iid, normal with variance = 1

# calculate posterior
# because the d-dimension prior distribution is iid, normal with variance 1, 
# the probability can be calculated using scipy.stats.norm(dist), 
# where dist is the Euclid distance between x and center
def calculatePosterior(x, centers, sigmas = False, weights = False):
    if not sigmas:
        sigmas = [1 for i in range(len(centers))]
    if not weights:
        weights = [1 for i in range(len(centers))]      
    posterior = []
    for center, sigma, weight in zip(centers, sigmas, weights):
        distance = EuclidDistance(x, center)/sigma
        posterior.append(stats.norm.pdf(distance)*weight)
    posterior = [post/sum(posterior) for post in posterior]
    maxind = posterior.index(max(posterior))
    '''
    if x[0] > 11 and x[1] > 11:
        print("Centers:", centers)
        print("Sigmas:", sigmas)
        print("Weights:", weights)
        print("x:", x)
        print("posterio:r", posterior)
    '''
    return({"posterior":posterior,"maxind":maxind})
# example
calculatePosterior([0], [[0],[1],[2],[3]], sigmas = [1,1,1,1])
calculatePosterior([0], [[0],[1],[2],[3]], sigmas = [1,1,1,1], weights = [1,0,0,0])
calculatePosterior([0,0], [[0,0],[1,1],[2,2],[3,3]])

def EM(k, xs, init = False, maxstep = 1000, minsteperr = 1e-3, plot = False, gifname = 'EM.gif'):
    n = len(xs)
    d = len(xs[0])
    #check if k is > len(xs)
    if k > n:
        print("Error: number of cluster is larger than data size.")
        return(-9999)
    # initialize center
    if init != False:
        centers = init['centers']
        sigmas = init['sigmas']
    else:
        centers_ind = random.sample(range(0, n),k) 
        centers = [xs[centers_ind[i]] for i in range(k)]
        sigmas = [1 for i in range(k)]
    weights = [float(1)/k for i in range(k)]

    step = 1
    error = math.inf
    steperror = math.inf

    #plot if plot = True and d = 2
    if plot and d == 2:
        cmap = plt.cm.get_cmap('hsv', k+5) 
        xx = [x[0] for x in xs]
        yy = [x[1] for x in xs]
        plt.plot(xx, yy, 'o', color=cmap(0), markerfacecolor='None')
        for i in range(k):
            plt.plot(centers[i][0], centers[i][1], '^', color=cmap(i), \
                     markersize = 20, markeredgecolor='k')
            plt.plot(centers[i][0], centers[i][1], '+k')
        plt.axis('equal')
        plt.title("step = 0")
        ax = plt.gca()
        text = ""
        for iic in range(k):
            text += "["
            for iid in range(d):
                text += " %.2f"%centers[iic][iid] + " "
            text += "] "
        ax.text(0.01, 0.95, text,
             verticalalignment='top', horizontalalignment='left',
                transform=ax.transAxes, color='black', fontsize=10)
        plt.savefig('images/EM_step_0.png')            
        plt.show()  
        # create gif
        images = []
        images.append(imageio.imread('images/EM_step_0.png'))

    posterior = [[] for i in range(n)]
    while (step < maxstep and steperror > minsteperr):
        clusters = [[] for i in range(k)]
        #E-step: calculate the k posterior probs for each point
        for idx, x in enumerate(xs):
            post = calculatePosterior(x, centers, sigmas)
            #sigmas = [1 for i in range(k)]
            posterior[idx] = post["posterior"]
            clusters[post["maxind"]].append(x)
        #M-step: calculate the new center using prob-weighted points averaging
        #centers_prob_sum is to keep the sum of prob.
        new_centers = [[0 for i in range(d)] for j in range(k)]
        new_sigmas = [0 for i in range(k)]
        weights = [0 for i in range(k)]
        #weights = list(reduce(lambda a, b: map(lambda c,d: c+d, a, b), posterior))
        #new_centers = list(reduce(lambda a,b: map(lambda c,d: c+d, a, b),map(lambda x, p: list(map(lambda xx, pp: xx*pp, x, p)), xs, posterior)))
        #print("here:", new_centers)
        
        # iix from 0 to n-1
        for iix, x in enumerate(xs):
            # iip from 0 to k-1
            for iip, prob in enumerate(posterior[iix]):
                weights[iip] += prob/float(n)
                # iid from 0 to d-1
                for iid in range(d):
                    new_centers[iip][iid] += prob*x[iid]
        
        for iip in range(k):
            new_centers[iip] = [new_center/(weights[iip]*n) for new_center in new_centers[iip]]
            new_sigmas[iip] = math.sqrt(reduce(lambda a, b: a+b, 
                      [EuclidDistance(x,new_centers[iip])**2*prob[iip]/(weights[iip]*n) for x, prob in zip(xs, posterior)]))
            #print("iip = ", iip, "; weights[iip] = ", sum([prob[iip] for prob in posterior])/float(n), "; weights[iip] =", weights[iip])
        #pdb.set_trace()
        #calculate the error term using new centers
        newerror = 0
        for idx, x in enumerate(xs):
            for idc, c in enumerate(new_centers):
                newerror += posterior[idx][idc]*EuclidDistance(x, c)
        newerror = newerror/float(n)
        if newerror == error:
            print("Error won't decrease")
            print(new_sigmas)
            #break
        else:
            centers = new_centers.copy()
            sigmas = new_sigmas.copy()
        
            steperror = error - newerror
            error = newerror
            print("step = %d; error = %.3f; steperror = %.3f"%(step, error, steperror))
            print("Centers: ", centers)
            print("Sigmas:", sigmas)
            
            #plot if plot = True and d = 2
            if plot and d == 2:
                #cmap = plt.cm.get_cmap('hsv', k+5) 
                for i in range(k):
                    xx = [x[0] for x in clusters[i]]
                    yy = [x[1] for x in clusters[i]]
                    plt.plot(xx, yy, 'o', color=cmap(i), markerfacecolor='None')
                    plt.plot(centers[i][0], centers[i][1], '^', color=cmap(i), markersize = 20, markeredgecolor='k')
                    plt.plot(centers[i][0], centers[i][1], '+k')
                plt.axis('equal')
                plt.title("step = %d, error = %.3f"%(step, error))
                ax = plt.gca()
                text = ""
                for iic in range(k):
                    text += "["
                    for iid in range(d):
                        text += " %.2f"%centers[iic][iid] + " "
                    text += "] "
                ax.text(0.01, 0.95, text,
                     verticalalignment='top', horizontalalignment='left',
                        transform=ax.transAxes, color='black', fontsize=10)
                plt.savefig('images/EM_step_%d.png'%step) 
                plt.show() 
                images.append(imageio.imread('images/EM_step_%d.png'%step))
            # update step
            step += 1
    if step == maxstep:
        print("Reached maximum Step Size %d"%maxstep)
    else:
        print("Error decrease size is less than the threshold of %f"%minsteperr)
    if plot and d==2:
        imageio.mimsave(gifname, images, fps = 1)
        
    return {"clusters":clusters, "centers":centers}
    


if __name__ == '__main__':   
    mean1, mean2, mean3 = [0, 0], [10,0], [5,10]
    cov1, cov2, cov3 = [[1, 0], [0, 1]], [[1, 0], [0, 1]], [[1, 0], [0, 1]]
    xs1, ys1 = np.random.multivariate_normal(mean1, cov1, 1000).T
    xs2, ys2 = np.random.multivariate_normal(mean2, cov2, 1000).T
    xs3, ys3 = np.random.multivariate_normal(mean3, cov3, 1000).T 
    ks = [[x,y] for x,y in zip(xs1,ys1)] + [[x,y] for x,y in zip(xs2,ys2)] + [[x,y] for x,y in zip(xs3,ys3)]
    # bad init
    #result = kmean(3, ks, init = {'centers':[[4,10],[6,10],[0.2,0]]}, maxstep = 25, \
    #               minsteperr = 1e-3, plot=True, gifname="kmean_k=3_bad_init.gif")
    #result = EM(3, ks, init = {'centers':[[4,10],[6,10],[0.2,0]], 'sigmas':[1,1,1]}, maxstep = 25, \
    #            minsteperr = 1e-3, plot=True, gifname="EM_k=3_bad_init.gif")

    # good init
    #result = kmean(3, ks, init = {'centers':[[4,10],[6,10],[0,0]]}, maxstep = 100, 
    #              minsteperr = 1e-3, plot=True, gifname="kmean_k=3_good_init.gif")

    #result = EM(3, ks, init = {'centers':[[4,10],[6,10],[0,0]], 'sigmas':[1,1,1]}, maxstep = 25, \
    #            minsteperr = 1e-3, plot=True, gifname="EM_k=3_good_init.gif")




    mean1, mean2, mean3 = [0, 0], [-5,5], [5,5]
    #mean1, mean2, mean3 = [0, 0], [-10,10], [10,10]
    cov1, cov2, cov3 = [[5, 0], [0, 5]], [[0.5, 0], [0, 0.5]], [[0.5, 0], [0, 0.5]]
    xs1, ys1 = np.random.multivariate_normal(mean1, cov1, 1000).T
    xs2, ys2 = np.random.multivariate_normal(mean2, cov2, 100).T
    xs3, ys3 = np.random.multivariate_normal(mean3, cov3, 100).T 
    ks = [[x,y] for x,y in zip(xs1,ys1)] + [[x,y] for x,y in zip(xs2,ys2)] + [[x,y] for x,y in zip(xs3,ys3)]

    cmap = plt.cm.get_cmap('hsv', 3+5) 
    plt.plot(xs1, ys1, 'x', color=cmap(0), markerfacecolor='None')
    plt.plot(xs2, ys2, 'o', color=cmap(1), markerfacecolor='None')
    plt.plot(xs3, ys3, '+', color=cmap(2), markerfacecolor='None')
    plt.axis('equal')
    plt.savefig('realdata_k=3_good.png') 
    plt.show()
    
    # good init
    result = kmean(3, ks, maxstep = 250, \
                   init = {'centers':[[2,2],[-4,4],[4.5,4.5]]},\
                  minsteperr = 1e-10, plot=True, gifname="kmean_k=3_good_init.gif")
    result = EM(3, ks, maxstep = 250, \
                init = {'centers':[[2,2],[-4,4],[4.5,4.5]], 'sigmas':[5,1,1]}, \
                minsteperr = 1e-10, plot=True, gifname="EM_k=3_good_init.gif")


    # bad init
    #result = kmean(3, ks, maxstep = 250, \
    #               init = {'centers':[[-2,-2],[-3,3],[3,3]]},\
    #              minsteperr = 1e-10, plot=True, gifname="kmean_k=3_bad_init.gif")
    #result = EM(3, ks, maxstep = 250, \
    #            init = {'centers':[[-2,-2],[-3,3],[3,3]], 'sigmas':[5,1,1]}, \
    #            minsteperr = 1e-10, plot=True, gifname="EM_k=3_bad_init.gif")


    # sensitive
    #result = kmean(3, ks, maxstep = 250, \
    #               init = {'centers':[[2,2],[-4,4],[4.0,4.0]]},\
    #              minsteperr = 1e-10, plot=True, gifname="kmean_k=3_sensitive_init.gif")
    #result = EM(3, ks, maxstep = 250, \
    #            init = {'centers':[[0,0],[-3.8,3.8],[4.5,4.5]], 'sigmas':[5,1,1]}, \
    #            minsteperr = 1e-10, plot=True, gifname="EM_k=3_sensitive_init.gif")
